// This will be prefixed to global resources as required to ensure ids are unique
export const orgResourcePrefix = '' || process.env.ORG_PREFIX

export const orgId = '' || process.env.GCP_ORG_ID
export const orgIdentifier = 'organizations/' + orgId

export const billingAccount = '' || process.env.GCP_BILLING_ACCOUNT

if(!orgResourcePrefix) throw 'ORG_PREFIX is required'
if(!orgId) throw 'GCP_ORG_ID is required'
if(!billingAccount) throw 'GCP_BILLING_ACCOUNT is required'
