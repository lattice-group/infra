import * as pulumi from "@pulumi/pulumi";
import * as gcp from "@pulumi/gcp";
import * as globals from '../globals'
import { Folder } from "@pulumi/gcp/organizations/folder";
import {Project} from "@pulumi/gcp/organizations";

// TODO create budget/alerts

// Setup the folder structure  ======

const prodFolder = new Folder('Prod', { displayName: 'Prod', parent: globals.orgIdentifier})

const nonProdFolder = new Folder('Non-Prod', { displayName: 'Non-Prod', parent: globals.orgIdentifier})
const e2eFolder = new Folder('CI-E2E', { displayName: 'CI-E2E', parent: nonProdFolder.id})

// Create a top-level project  =======

const orgLevelProjectId = `${globals.orgResourcePrefix}-org`
const orgLevelProject = new Project(orgLevelProjectId,
    {projectId: orgLevelProjectId, orgId: globals.orgId, billingAccount: globals.billingAccount, autoCreateNetwork: false})


// Create a service account that can administer all resources under the CI-E2E folder =====

const e2eServiceAccount = new gcp.serviceAccount.Account('e2e-admin',
    { project: orgLevelProject.id, accountId: 'e2e-admin', displayName: 'Service account to administer temporary projects for end-to-end integration tests'})

const e2eAdminIAM = new gcp.folder.IAMBinding('e2e-admin',
    { folder: e2eFolder.name, members: [pulumi.interpolate `serviceAccount:${e2eServiceAccount.email}`], role: 'roles/resourcemanager.folderAdmin'});
